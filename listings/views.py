from django.shortcuts import get_object_or_404, render
from .models import Listing
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from .choices import price_choices, bedroom_choices, state_choices


# fetch the listings using the model and insert into template, loop and output from db
# view render methods for html templates
def index(request):  # data passed into view template
    # fetch listing from db
    listings = Listing.objects.order_by("-list_date").filter(
        is_published=True
    )  # sort by and remove unpublished
    # listings = Listing.objects.all()

    paginator = Paginator(listings, 3)  # per page
    page = request.GET.get("page")  # url param
    paged_listings = paginator.get_page(page)

    context = {"listings": paged_listings}
    return render(request, "listings/listings.html", context)


def listing(request, listing_id):
    # display listing or 404 if listing id doesn't exist
    listing = get_object_or_404(Listing, pk=listing_id)
    context = {"listing": listing}

    return render(request, "listings/listing.html", context)


# check first to see if the field exists, pull it out of the request and put into variable and base our query on the variable
def search(request):
    queryset_list = Listing.objects.order_by("-list_date")

    # Keywords
    if "keywords" in request.GET:
        keywords = request.GET["keywords"]
        if keywords:
            queryset_list = queryset_list.filter(
                description__icontains=keywords
            )  # check for words typed by the user

    # City
    if "city" in request.GET:
        city = request.GET["city"]
        if city:
            queryset_list = queryset_list.filter(city__iexact=city)

    # State
    if "state" in request.GET:
        state = request.GET["state"]
        if state:
            queryset_list = queryset_list.filter(state__iexact=state)

    # Bedrooms
    if "bedrooms" in request.GET:
        bedrooms = request.GET["bedrooms"]
        if bedrooms:
            queryset_list = queryset_list.filter(bedrooms__lte=bedrooms)

    # Price
    if "price" in request.GET:
        price = request.GET["price"]
        if price:
            queryset_list = queryset_list.filter(price__lte=price)

    context = {
        "state_choices": state_choices,
        "bedroom_choices": bedroom_choices,
        "price_choices": price_choices,
        "listings": queryset_list,
        "values": request.GET,  # persist form states on submit
    }

    return render(request, "listings/search.html", context)
