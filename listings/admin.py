from django.contrib import admin

from .models import Listing


# custom admin display data - shows a table with the below data
class ListingAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "is_published", "price", "list_date", "realtor")
    list_display_links = ("id", "title")  # allows id/title to be clickable
    list_filter = ("realtor",)  # added side filter - filter by realtor
    list_editable = ("is_published",)  # isPublished checkbox
    # searchable fields
    search_fields = (
        "title",
        "description",
        "address",
        "city",
        "state",
        "zipcode",
        "price",
    )
    list_per_page = 25


admin.site.register(Listing, ListingAdmin)  # models to show in django admin
