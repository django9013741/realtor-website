const date = new Date();
document.querySelector(".year").innerHTML = date.getFullYear();

// timeout alert message
setTimeout(() => {
  $("#message").fadeOut("slow");
}, 3000);
