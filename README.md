<h1 align="center">
<br>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/python/python-original.svg" width="80px"  />       
<br>
<br>
Django Realtor Website
</h1>

<h4 align="center">Login/Register/Search/Paginate</h4>
<p align="center">Fullstack app built with django and Jinja template engine</p>

<div align="center">
   <img align="center" src="./github-imgs/app1.png" width="500px">
   <img align="center" src="./github-imgs/app2.png" width="500px">
    <img align="center" src="./github-imgs/app3.png" width="300px">
     <img align="center" src="./github-imgs/app4.png" width="400px">
</div>
