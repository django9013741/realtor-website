from django.shortcuts import render
from django.http import HttpResponse
from listings.choices import price_choices, bedroom_choices, state_choices

from listings.models import Listing
from realtors.models import Realtor


# view render methods for html templates - root page
def index(request):
    listings = Listing.objects.order_by("-list_date").filter(is_published=True)[
        :3
    ]  # sort listings by newest, remove unpublished and limit to 3
    context = {
        "listings": listings,
        "state_choices": state_choices,
        "bedroom_choices": bedroom_choices,
        "price_choices": price_choices,
    }

    return render(request, "pages/index.html", context)
    # return HttpResponse("<h1>Hello World</h1>")


def about(request):
    # Get all realtors
    realtors = Realtor.objects.order_by("-hire_date")
    # GET MVP - seller of the month has mvp check
    mvp_realtors = Realtor.objects.all().filter(is_mvp=True)
    context = {"realtors": realtors, "mvp_realtors": mvp_realtors}

    return render(request, "pages/about.html", context)
