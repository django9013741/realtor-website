from django.urls import path
from . import views

# home page url
urlpatterns = [
    path("", views.index, name="index"),  # root path '/'
    path("about", views.about, name="about"),
]
